#ifndef _GLOOPS_TEXTURES_
#define _GLOOPS_TEXTURES_

#include "../GLObject.hpp"
#include "../states.hpp"

namespace gloops {

namespace details {
    struct TextureGenerator {
        static GLuint genObject() {
            GLuint id;
            glGenTextures(1, &id);
            return id;
        }
        
        static void deleteObject(GLuint id) {
            glDeleteTextures(1, &id);
        }
    };
}

typedef GLObject<details::TextureGenerator> TextureObject;

class TextureBind {
public:
    TextureBind(GLenum target, GLint unit, const TextureObject& texture):
        m_Target(target), m_Unit(unit) {
        glActiveTexture(GL_TEXTURE0 + m_Unit);
        GLOOPS_CHECK_ERROR;
        glBindTexture(target, texture.glId());
        GLOOPS_CHECK_ERROR;
    }
    
    ~TextureBind() {
        glActiveTexture(GL_TEXTURE0 + m_Unit);
        GLOOPS_CHECK_ERROR;
        glBindTexture(m_Target, 0);
        GLOOPS_CHECK_ERROR;
    }
    
    TextureBind(const TextureBind&) = delete;
    TextureBind& operator =(const TextureBind&) = delete;
    
    GLint getTextureUnit() const {
        return m_Unit;
    }
    
    TextureBind& texImage1D(
        GLint level,
        GLint internalFormat,
        GLsizei width,
        GLint border,
        GLenum format,
        GLenum type,
        const GLvoid* data) 
    {
        glTexImage1D(
            m_Target,
            level,
            internalFormat,
            width,
            border,
            format,
            type,
            data);
        GLOOPS_CHECK_ERROR;
        return *this;
    }
    
    TextureBind& texImage2D(
        GLint level,
        GLint internalFormat,
        GLsizei width,
        GLsizei height,
        GLint border,
        GLenum format,
        GLenum type,
        const GLvoid* data) 
    {
        glTexImage2D(
            m_Target,
            level,
            internalFormat,
            width,
            height,
            border,
            format,
            type,
            data);
        GLOOPS_CHECK_ERROR;
        return *this;
    }
    
    TextureBind& texParameter(GLenum pname, GLint param) {
        glTexParameteri(m_Target, pname, param);
        GLOOPS_CHECK_ERROR;
        return *this;
    }
    
    TextureBind& texParameter(GLenum pname, GLfloat param) {
        glTexParameterf(m_Target, pname, param);
        GLOOPS_CHECK_ERROR;
        return *this;
    }
    
    TextureBind& texParameter(GLenum pname, const GLint* params) {
        glTexParameteriv(m_Target, pname, params);
        GLOOPS_CHECK_ERROR;
        return *this;
    }
    
    TextureBind& texParameter(GLenum pname, const GLfloat* params) {
        glTexParameterfv(m_Target, pname, params);
        GLOOPS_CHECK_ERROR;
        return *this;
    }
    
    TextureBind& setMinFilter(GLint filter) {
        return texParameter(GL_TEXTURE_MIN_FILTER, filter);
    }
    
    TextureBind& setMagFilter(GLint filter) {
        return texParameter(GL_TEXTURE_MAG_FILTER, filter);
    }

    TextureBind& generateMipmap() {
        glGenerateMipmap(m_Target);
        GLOOPS_CHECK_ERROR;
        return *this;
    }

private:
    GLenum m_Target;
    GLint m_Unit;
};

// Save the active texture unit and restore it at destruction
template<>
struct StateRestorer<GL_ACTIVE_TEXTURE> {
    GLint unit;

    StateRestorer() {
        glGetIntegerv(GL_ACTIVE_TEXTURE, &unit);
    }
    
    ~StateRestorer() {
        glActiveTexture(unit);
    }
    
};

}

#endif
