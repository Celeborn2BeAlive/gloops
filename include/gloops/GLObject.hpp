#ifndef _GLOOPS_GLOBJECT_
#define _GLOOPS_GLOBJECT_

#include "GL.hpp"
#include "errors.hpp"

namespace gloops {
    
template<typename Generator>
class GLObject {
public:
    GLObject():
        m_nGLid(Generator::genObject()) {
        GLOOPS_CHECK_ERROR;
    }

    ~GLObject() {
        Generator::deleteObject(m_nGLid);
        GLOOPS_CHECK_ERROR;
    }
    
    GLObject(const GLObject&) = delete;
    
    GLObject& operator =(const GLObject&) = delete;
    
    GLObject(GLObject&& object): m_nGLid(object.m_nGLid) {
        object.m_nGLid = 0;
    }
    
    GLObject& operator =(GLObject&& object) {
        this->~GLObject();
        m_nGLid = object.m_nGLid;
        object.m_nGLid = 0;
        return *this;
    }

    GLuint glId() const {
        return m_nGLid;
    }

private:
    GLuint m_nGLid;
};

/*
template<typename GLObject>
class Bind;

template<typename GLObject>
inline Bind<GLObject> bind(GLenum target, GLObject& object) {
    return Bind<GLObject>(target, object);
}

template<typename GLObject>
inline Bind<GLObject> bind(GLObject& object) {
    return Bind<GLObject>(object);
}
*/

}

#endif
