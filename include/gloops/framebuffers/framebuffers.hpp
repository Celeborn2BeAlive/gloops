#ifndef _GLOOPS_FRAMEBUFFERS_
#define _GLOOPS_FRAMEBUFFERS_

#include "../GLObject.hpp"
#include "../textures/textures.hpp"

namespace gloops {
    
namespace details {
    struct FramebufferGenerator {
        static GLuint genObject() {
            GLuint id;
            glGenFramebuffers(1, &id);
            GLOOPS_CHECK_ERROR;
            return id;
        }
        
        static void deleteObject(GLuint id) {
            glDeleteFramebuffers(1, &id);
            GLOOPS_CHECK_ERROR;
        }
    };
}

typedef GLObject<details::FramebufferGenerator> FramebufferObject;

class FramebufferBind {
public:
    FramebufferBind(GLenum target, const FramebufferObject& fbo):
        m_Target(target) {
        glBindFramebuffer(target, fbo.glId());
        GLOOPS_CHECK_ERROR;
    }
    
    ~FramebufferBind() {
        glBindFramebuffer(m_Target, 0);
        GLOOPS_CHECK_ERROR;
    }
    
    FramebufferBind(const FramebufferBind&) = delete;
    FramebufferBind& operator =(const FramebufferBind&) = delete;

    FramebufferBind& framebufferTexture2D(
        GLenum attachment,
        GLenum textarget,
        const TextureObject& texture,
        GLint level) {
        glFramebufferTexture2D(m_Target, attachment, textarget, texture.glId(), level);
        GLOOPS_CHECK_ERROR;
        return *this;
    }
    
    GLenum checkFramebufferStatus() {
        GLenum value = glCheckFramebufferStatus(m_Target);
        GLOOPS_CHECK_ERROR;
        return value;
    }

private:
    GLenum m_Target;
};

inline const char* framebufferErrorString(GLenum error) {
    switch(error) {
    case GL_FRAMEBUFFER_COMPLETE:
        return "GL_FRAMEBUFFER_COMPLETE: the framebuffer is complete.";
    case GL_FRAMEBUFFER_UNDEFINED:
        return "GL_FRAMEBUFFER_UNDEFINED: the default framebuffer does not exist.";
    case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
        return "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT: any of the framebuffer attachment "
        "points are framebuffer incomplete.";
    case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
        return "GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT: the framebuffer does not "
        "have at least one image attached to it.";
    case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
        return "GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER: the value of"
        "GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE is GL_NONE for any color attachment point(s) "
        "named by GL_DRAWBUFFERi.";
    case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
        return "GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER: GL_READ_BUFFER is not GL_NONE and "
        "the value of GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE is GL_NONE for the color attachment "
        "point named by GL_READ_BUFFER.";
    case GL_FRAMEBUFFER_UNSUPPORTED:
        return "GL_FRAMEBUFFER_UNSUPPORTED: the combination of internal formats of the attached "
        "images violates an implementation-dependent set of restrictions.";
    case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
        return "GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE: the value of GL_RENDERBUFFER_SAMPLES is "
        "not the same for all attached renderbuffers; OR the value of GL_TEXTURE_SAMPLES is the "
        "not same for all attached textures; OR the attached images are a mix of renderbuffers "
        "and textures, the value of GL_RENDERBUFFER_SAMPLES does not match the value of GL_TEXTURE_SAMPLES. "
        "OR the value of GL_TEXTURE_FIXED_SAMPLE_LOCATIONS is not the same for all attached textures; "
        "OR the attached images are a mix of renderbuffers and textures, the value of "
        "GL_TEXTURE_FIXED_SAMPLE_LOCATIONS is not GL_TRUE for all attached textures.";
    case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
        return "GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS: any framebuffer attachment is layered, "
        "and any populated attachment is not layered, or if all populated color attachments are not "
        "from textures of the same target.";
    }
    
    return "";
}

}

#endif
