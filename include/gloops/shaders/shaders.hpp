#ifndef _GLOOPS_SHADERS
#define _GLOOPS_SHADERS

#include <memory>

#include "../GL.hpp"
#include "../errors.hpp"
#include "../states.hpp"

// Macro for stringifying shader source code
#define GLOOPS_STRINGIFY(str) #str

namespace gloops {

typedef std::unique_ptr<char[]> CharBuffer;

class Shader {
public:
    Shader(GLenum shaderType):
        m_nGLid(glCreateShader(shaderType)) {
        GLOOPS_CHECK_ERROR;
    }
        
    ~Shader() {
        glDeleteShader(m_nGLid);
        GLOOPS_CHECK_ERROR;
    }
    
    Shader(const Shader&) = delete;
    
    Shader& operator =(const Shader&) = delete;
    
    Shader(Shader&& shader): m_nGLid(shader.m_nGLid) {
        shader.m_nGLid = 0;
    }
    
    Shader& operator =(Shader&& shader) {
        this->~Shader();
        m_nGLid = shader.m_nGLid;
        shader.m_nGLid = 0;
        return *this;
    }
    
    GLuint glId() const {
        return m_nGLid;
    }
    
    void setSource(const GLchar* src) {
        glShaderSource(m_nGLid, 1, &src, 0);
        GLOOPS_CHECK_ERROR;
    }

    void setSource(const std::string& src) {
        setSource(src.c_str());
    }
    
    bool compile() {
        glCompileShader(m_nGLid);
        GLOOPS_CHECK_ERROR;
        return getCompileStatus();
    }
    
    bool getCompileStatus() const {
        GLint status;
        glGetShaderiv(m_nGLid, GL_COMPILE_STATUS, &status);
        GLOOPS_CHECK_ERROR;
        return status == GL_TRUE;
    }
    
    CharBuffer getInfoLog() const {
        GLint logLength;
        glGetShaderiv(m_nGLid, GL_INFO_LOG_LENGTH, &logLength);
        GLOOPS_CHECK_ERROR;
        
        CharBuffer buffer(new char[logLength]);
        glGetShaderInfoLog(m_nGLid, logLength, 0, buffer.get());
        GLOOPS_CHECK_ERROR;
        
        return buffer;
    }
    
private:
    GLuint m_nGLid;
};

class Program {
public:
    Program():
        m_nGLid(glCreateProgram()) {
        GLOOPS_CHECK_ERROR;
    }

    ~Program() {
        glDeleteProgram(m_nGLid);
        GLOOPS_CHECK_ERROR;
    }
    
    Program(const Program&) = delete;
    
    Program& operator =(const Program&) = delete;
    
    Program(Program&& program): m_nGLid(program.m_nGLid) {
        program.m_nGLid = 0;
    }
    
    Program& operator =(Program&& program) {
        this->~Program();
        m_nGLid = program.m_nGLid;
        program.m_nGLid = 0;
        return *this;
    }
    
    GLuint glId() const {
        return m_nGLid;
    }
    
    void attachShader(const Shader& shader) {
        glAttachShader(m_nGLid, shader.glId());
        GLOOPS_CHECK_ERROR;
    }
    
    bool link() {
        glLinkProgram(m_nGLid);
        GLOOPS_CHECK_ERROR;
        return getLinkStatus();
    }
    
    bool getLinkStatus() const {
        GLint linkStatus;
        glGetProgramiv(m_nGLid, GL_LINK_STATUS, &linkStatus);
        GLOOPS_CHECK_ERROR;
        return linkStatus == GL_TRUE;
    }
    
    CharBuffer getInfoLog() const {
        GLint logLength;
        glGetProgramiv(m_nGLid, GL_INFO_LOG_LENGTH, &logLength);
        GLOOPS_CHECK_ERROR;
        
        CharBuffer buffer(new char[logLength]);
        glGetProgramInfoLog(m_nGLid, logLength, 0, buffer.get());
        GLOOPS_CHECK_ERROR;
        
        return buffer;
    }
    
    void use() const {
        glUseProgram(m_nGLid);
        GLOOPS_CHECK_ERROR;
    }

    GLint getUniformLocation(const GLchar* name) const {
        GLint location = glGetUniformLocation(m_nGLid, name);
        GLOOPS_CHECK_ERROR;
        return location;
    }
    
    GLint getAttribLocation(const GLchar* name) const {
        GLint location = glGetAttribLocation(m_nGLid, name);
        GLOOPS_CHECK_ERROR;
        return location;
    }
    
    void bindAttribLocation(GLuint index, const GLchar* name) const {
        glBindAttribLocation(m_nGLid, index, name);
        GLOOPS_CHECK_ERROR;
    }

private:
    GLuint m_nGLid;
};

// Save the active program restore it at destruction
template<>
struct StateRestorer<GL_CURRENT_PROGRAM> {
    GLint program;

    StateRestorer() {
        glGetIntegerv(GL_CURRENT_PROGRAM, &program);
    }
    
    ~StateRestorer() {
        glUseProgram(program);
    }
    
};

}

#endif
