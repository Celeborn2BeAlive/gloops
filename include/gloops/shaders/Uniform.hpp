#ifndef _GLOOPS_UNIFORM
#define _GLOOPS_UNIFORM

#include <iostream>
#include "shaders.hpp"

#ifdef GLOOPS_USE_GLM

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#endif

namespace gloops {

class Uniform {
public:
    Uniform():
        m_nLocation(-1) {
    }

    Uniform(const Program& program, const GLchar* name, bool checkIfExists = false):
        m_nLocation(program.getUniformLocation(name)) {
        GLOOPS_CHECK_ERROR;
        if(checkIfExists && m_nLocation < 0) {
            std::clog << "gloops warning: uniform '" << name << "' not found in the program "
                << program.glId() << std::endl;
        }
    }
    
    GLint location() const {
        return m_nLocation;
    }
    
    void set(GLfloat v0) {
        glUniform1f(m_nLocation, v0);
        GLOOPS_CHECK_ERROR;
    }
    
    void set(GLfloat v0, GLfloat v1) {
        glUniform2f(m_nLocation, v0, v1);
        GLOOPS_CHECK_ERROR;
    }
    
    void set(GLfloat v0, GLfloat v1, GLfloat v2) {
        glUniform3f(m_nLocation, v0, v1, v2);
        GLOOPS_CHECK_ERROR;
    }
    
    void set(GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3) {
        glUniform4f(m_nLocation, v0, v1, v2, v3);
        GLOOPS_CHECK_ERROR;
    }
    
    void set(GLint v0) {
        glUniform1i(m_nLocation, v0);
        GLOOPS_CHECK_ERROR;
    }
    
    void set(GLint v0, GLint v1) {
        glUniform2i(m_nLocation, v0, v1);
        GLOOPS_CHECK_ERROR;
    }
    
    void set(GLint v0, GLint v1, GLint v2) {
        glUniform3i(m_nLocation, v0, v1, v2);
        GLOOPS_CHECK_ERROR;
    }
    
    void set(GLint v0, GLint v1, GLint v2, GLint v3) {
        glUniform4i(m_nLocation, v0, v1, v2, v3);
        GLOOPS_CHECK_ERROR;
    }

    void set(GLuint64 v0) {
        glUniform1ui64NV(m_nLocation, v0);
    }
    
    void set(GLuint v0) {
        glUniform1ui(m_nLocation, v0);
        GLOOPS_CHECK_ERROR;
    }
    
    void set(GLuint v0, GLuint v1) {
        glUniform2ui(m_nLocation, v0, v1);
        GLOOPS_CHECK_ERROR;
    }
    
    void set(GLuint v0, GLuint v1, GLuint v2) {
        glUniform3ui(m_nLocation, v0, v1, v2);
        GLOOPS_CHECK_ERROR;
    }
    
    void set(GLuint v0, GLuint v1, GLuint v2, GLuint v3) {
        glUniform4ui(m_nLocation, v0, v1, v2, v3);
        GLOOPS_CHECK_ERROR;
    }
    
    void set1v(GLsizei count, const GLfloat* value) {
        glUniform1fv(m_nLocation, count, value);
        GLOOPS_CHECK_ERROR;
    }
    
    void set2v(GLsizei count, const GLfloat* value) {
        glUniform2fv(m_nLocation, count, value);
        GLOOPS_CHECK_ERROR;
    }
    
    void set3v(GLsizei count, const GLfloat* value) {
        glUniform3fv(m_nLocation, count, value);
        GLOOPS_CHECK_ERROR;
    }
    
    void set4v(GLsizei count, const GLfloat* value) {
        glUniform4fv(m_nLocation, count, value);
        GLOOPS_CHECK_ERROR;
    }
    
    void set1v(GLsizei count, const GLint* value) {
        glUniform1iv(m_nLocation, count, value);
        GLOOPS_CHECK_ERROR;
    }
    
    void set2v(GLsizei count, const GLint* value) {
        glUniform2iv(m_nLocation, count, value);
        GLOOPS_CHECK_ERROR;
    }
    
    void set3v(GLsizei count, const GLint* value) {
        glUniform3iv(m_nLocation, count, value);
        GLOOPS_CHECK_ERROR;
    }
    
    void set4v(GLsizei count, const GLint* value) {
        glUniform4iv(m_nLocation, count, value);
        GLOOPS_CHECK_ERROR;
    }
    
    void set1v(GLsizei count, const GLuint* value) {
        glUniform1uiv(m_nLocation, count, value);
        GLOOPS_CHECK_ERROR;
    }
    
    void set2v(GLsizei count, const GLuint* value) {
        glUniform2uiv(m_nLocation, count, value);
        GLOOPS_CHECK_ERROR;
    }
    
    void set3v(GLsizei count, const GLuint* value) {
        glUniform3uiv(m_nLocation, count, value);
        GLOOPS_CHECK_ERROR;
    }
    
    void set4v(GLsizei count, const GLuint* value) {
        glUniform4uiv(m_nLocation, count, value);
        GLOOPS_CHECK_ERROR;
    }
    
    void setMatrix2(GLsizei count, GLboolean transpose, const GLfloat* value) {
        glUniformMatrix2fv(m_nLocation, count, transpose, value);
        GLOOPS_CHECK_ERROR;
    }
    
    void setMatrix3(GLsizei count, GLboolean transpose, const GLfloat* value) {
        glUniformMatrix3fv(m_nLocation, count, transpose, value);
        GLOOPS_CHECK_ERROR;
    }
    
    void setMatrix4(GLsizei count, GLboolean transpose, const GLfloat* value) {
        glUniformMatrix4fv(m_nLocation, count, transpose, value);
        GLOOPS_CHECK_ERROR;
    }
    
    void setMatrix2x3(GLsizei count, GLboolean transpose, const GLfloat* value) {
        glUniformMatrix2x3fv(m_nLocation, count, transpose, value);
        GLOOPS_CHECK_ERROR;
    }
    
    void setMatrix3x2(GLsizei count, GLboolean transpose, const GLfloat* value) {
        glUniformMatrix3x2fv(m_nLocation, count, transpose, value);
        GLOOPS_CHECK_ERROR;
    }
    
    void setMatrix2x4(GLsizei count, GLboolean transpose, const GLfloat* value) {
        glUniformMatrix2x4fv(m_nLocation, count, transpose, value);
        GLOOPS_CHECK_ERROR;
    }
    
    void setMatrix4x2(GLsizei count, GLboolean transpose, const GLfloat* value) {
        glUniformMatrix4x2fv(m_nLocation, count, transpose, value);
        GLOOPS_CHECK_ERROR;
    }
    
    void setMatrix3x4(GLsizei count, GLboolean transpose, const GLfloat* value) {
        glUniformMatrix3x4fv(m_nLocation, count, transpose, value);
        GLOOPS_CHECK_ERROR;
    }
    
    void setMatrix4x3(GLsizei count, GLboolean transpose, const GLfloat* value) {
        glUniformMatrix4x3fv(m_nLocation, count, transpose, value);
        GLOOPS_CHECK_ERROR;
    }
    
#ifdef GLOOPS_USE_GLM

    void set(const glm::vec2& value) {
        set(value.x, value.y);
    }
    
    void set(const glm::vec3& value) {
        set(value.x, value.y, value.z);
    }
    
    void set(const glm::vec4& value) {
        set(value.x, value.y, value.z, value.w);
    }
    
    void set(const glm::ivec2& value) {
        set(value.x, value.y);
    }
    
    void set(const glm::ivec3& value) {
        set(value.x, value.y, value.z);
    }
    
    void set(const glm::ivec4& value) {
        set(value.x, value.y, value.z, value.w);
    }
    
    void set(const glm::uvec2& value) {
        set(value.x, value.y);
    }
    
    void set(const glm::uvec3& value) {
        set(value.x, value.y, value.z);
    }
    
    void set(const glm::uvec4& value) {
        set(value.x, value.y, value.z, value.w);
    }
    
    void set(const glm::mat2& value, bool transpose = false) {
        setMatrix2(1, transpose, glm::value_ptr(value));
    }
    
    void set(const glm::mat3& value, bool transpose = false) {
        setMatrix3(1, transpose, glm::value_ptr(value));
    }
    
    void set(const glm::mat4& value, bool transpose = false) {
        setMatrix4(1, transpose, glm::value_ptr(value));
    }
    
    void set(const glm::mat2x3& value, bool transpose = false) {
        setMatrix2x3(1, transpose, glm::value_ptr(value));
    }
    
    void set(const glm::mat3x2& value, bool transpose = false) {
        setMatrix3x2(1, transpose, glm::value_ptr(value));
    }
    
    void set(const glm::mat2x4& value, bool transpose = false) {
        setMatrix2x4(1, transpose, glm::value_ptr(value));
    }
    
    void set(const glm::mat4x2& value, bool transpose = false) {
        setMatrix4x2(1, transpose, glm::value_ptr(value));
    }
    
    void set(const glm::mat3x4& value, bool transpose = false) {
        setMatrix3x4(1, transpose, glm::value_ptr(value));
    }
    
    void set(const glm::mat4x3& value, bool transpose = false) {
        setMatrix4x3(1, transpose, glm::value_ptr(value));
    }

#endif
    
private:
    GLint m_nLocation;
};

}

#endif
