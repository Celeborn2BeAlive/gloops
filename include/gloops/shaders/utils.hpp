#ifndef _GLOOPS_SHADERS_UTILS_
#define _GLOOPS_SHADERS_UTILS_

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

#include "shaders.hpp"

namespace gloops {

inline bool loadShader(const std::string& filename, Shader& shader) {
    std::ifstream input(filename);
    if(!input) {
        return false;
    }
    
    std::stringstream buffer;
    buffer << input.rdbuf();
    
    shader.setSource(buffer.str().c_str());

    return true;
}

inline bool loadAndBuildProgram(const std::string& vsFilepath, const std::string& fsFilepath, Program& program) {
    Shader vs(GL_VERTEX_SHADER);
    
    if(!loadShader(vsFilepath, vs)) {
        std::cerr << "gloops::loadAndBuildProgram error :" << vsFilepath << "not found." << std::endl;
        return false;
    }
    
    if(!vs.compile()) {
        std::cerr << "gloops::loadAndBuildProgram error : vertex shader compilation error. Info log: " << std::endl
            << vs.getInfoLog().get() << std::endl;
        return false;
    }
    
    Shader fs(GL_FRAGMENT_SHADER);
    
    if(!loadShader(fsFilepath, fs)) {
        std::cerr << "gloops::loadAndBuildProgram error :" << fsFilepath << "not found." << std::endl;
        return false;
    }
    
    if(!fs.compile()) {
        std::cerr << "gloops::loadAndBuildProgram error : fragment shader compilation error. Info log: " << std::endl
            << fs.getInfoLog().get() << std::endl;
        return false;
    }
    
    program.attachShader(vs);
    program.attachShader(fs);
    
    if(!program.link()) {
        std::cerr << "gloops::loadAndBuildProgram error : program link error: Info log: " << std::endl
            << program.getInfoLog().get() << std::endl;
        return false;
    }
    
    return true;
}

template<typename VertexShaderSourceType, typename FragmentShaderSourceType>
inline bool buildProgram(VertexShaderSourceType&& vsSource, FragmentShaderSourceType&& fsSource, Program& program) {
    Shader vs(GL_VERTEX_SHADER);
    vs.setSource(std::forward<VertexShaderSourceType>(vsSource));

    if(!vs.compile()) {
        std::cerr << "gloops::buildProgram error : vertex shader compilation error. Info log: " << std::endl
            << vs.getInfoLog().get() << std::endl;
        return false;
    }

    Shader fs(GL_FRAGMENT_SHADER);
    fs.setSource(std::forward<FragmentShaderSourceType>(fsSource));

    if(!fs.compile()) {
        std::cerr << "gloops::buildProgram error : fragment shader compilation error. Info log: " << std::endl
            << fs.getInfoLog().get() << std::endl;
        return false;
    }

    program.attachShader(vs);
    program.attachShader(fs);

    if(!program.link()) {
        std::cerr << "gloops::buildProgram error : program link error: Info log: " << std::endl
            << program.getInfoLog().get() << std::endl;
        return false;
    }

    return true;
}

template<typename VertexShaderSourceType, typename GeometryShaderSourceType, typename FragmentShaderSourceType>
inline bool buildProgram(VertexShaderSourceType&& vsSource, GeometryShaderSourceType&& gsSource, FragmentShaderSourceType&& fsSource, Program& program) {
    Shader vs(GL_VERTEX_SHADER);
    vs.setSource(std::forward<VertexShaderSourceType>(vsSource));

    if(!vs.compile()) {
        std::cerr << "gloops::buildProgram error : vertex shader compilation error. Info log: " << std::endl
            << vs.getInfoLog().get() << std::endl;
        return false;
    }

    Shader gs(GL_GEOMETRY_SHADER);
    gs.setSource(std::forward<GeometryShaderSourceType>(gsSource));

    if(!gs.compile()) {
        std::cerr << "gloops::buildProgram error : geometry shader compilation error. Info log: " << std::endl
            << gs.getInfoLog().get() << std::endl;
        return false;
    }

    Shader fs(GL_FRAGMENT_SHADER);
    fs.setSource(std::forward<FragmentShaderSourceType>(fsSource));

    if(!fs.compile()) {
        std::cerr << "gloops::buildProgram error : fragment shader compilation error. Info log: " << std::endl
            << fs.getInfoLog().get() << std::endl;
        return false;
    }

    program.attachShader(vs);
    program.attachShader(gs);
    program.attachShader(fs);

    if(!program.link()) {
        std::cerr << "gloops::buildProgram error : program link error: Info log: " << std::endl
            << program.getInfoLog().get() << std::endl;
        return false;
    }

    return true;
}

}

#endif
