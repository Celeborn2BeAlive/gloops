#ifndef _GLOOPS_BUFFERS_
#define _GLOOPS_BUFFERS_

#include "../GLObject.hpp"

namespace gloops {
    
namespace details {
    struct BufferGenerator {
        static GLuint genObject() {
            GLuint id;
            glGenBuffers(1, &id);
            GLOOPS_CHECK_ERROR;
            return id;
        }
        
        static void deleteObject(GLuint id) {
            glDeleteBuffers(1, &id);
            GLOOPS_CHECK_ERROR;
        }
    };
}

typedef GLObject<details::BufferGenerator> BufferObject;

class BufferBind {
public:
    BufferBind(GLenum target, const BufferObject& buffer):
        m_Target(target) {
        glBindBuffer(target, buffer.glId());
        GLOOPS_CHECK_ERROR;
    }
    
    ~BufferBind() {
        glBindBuffer(m_Target, 0);
        GLOOPS_CHECK_ERROR;
    }
    
    BufferBind(const BufferBind&) = delete;
    BufferBind& operator =(const BufferBind&) = delete;
    
    void bind(const BufferObject& buffer) {
        glBindBuffer(m_Target, buffer.glId());
        GLOOPS_CHECK_ERROR;
    }
    
    BufferBind& bufferData(GLsizeiptr size, const GLvoid* data, GLenum usage) {
        glBufferData(m_Target, size, data, usage);
        GLOOPS_CHECK_ERROR;
        return *this;
    }
    
    template<typename T>
    BufferBind& bufferData(GLsizeiptr count, const T* data, GLenum usage) {
        glBufferData(m_Target, count * sizeof(T), data, usage);
        GLOOPS_CHECK_ERROR;
        return *this;
    }
    

private:
    GLenum m_Target;
};

}

#endif
