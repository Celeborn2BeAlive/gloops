#ifndef _GLOOPS_
#define _GLOOPS_

#include "GL.hpp"
#include "shaders/shaders.hpp"
#include "shaders/Uniform.hpp"
#include "shaders/utils.hpp"
#include "buffers/buffers.hpp"
#include "textures/textures.hpp"
#include "vertex_arrays/vertex_arrays.hpp"
#include "framebuffers/framebuffers.hpp"
#include "states.hpp"

namespace gloops {

static const uint32_t MAJOR_VERSION = 0;
static const uint32_t MINOR_VERSION = 1;

class CapabilityRestorer {
public:
    CapabilityRestorer(GLenum cap, bool enable): m_Cap(cap), m_OldValue(glIsEnabled(cap)) {
        if(enable) {
            glEnable(m_Cap);
        } else {
            glDisable(m_Cap);
        }
    }
    
    ~CapabilityRestorer() {
        if(m_OldValue) {
            glEnable(m_Cap);
        } else {
            glDisable(m_Cap);
        }
    }
    
private:
    GLenum m_Cap;
    GLboolean m_OldValue;
};

struct Viewport {
    GLint x, y;
    GLsizei width, height;
    
    Viewport() {
    }
    
    Viewport(GLint x, GLint y, GLsizei width, GLsizei height):
        x(x), y(y), width(width), height(height) {
    }
};

inline Viewport getViewport() {
    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    return Viewport(viewport[0], viewport[1], viewport[2], viewport[3]);
}

inline void setViewport(const Viewport& viewport) {
    glViewport(viewport.x, viewport.y, viewport.width, viewport.height);
}

template<>
struct StateRestorer<GL_LINE_WIDTH> {
    GLfloat lineWidth;

    StateRestorer() {
        glGetFloatv(GL_LINE_WIDTH, &lineWidth);
    }

    ~StateRestorer() {
        glLineWidth(lineWidth);
    }
};

template<>
struct StateRestorer<GL_VIEWPORT> {
    gloops::Viewport m_Viewport;

    StateRestorer():
        m_Viewport(getViewport()) {
    }

    ~StateRestorer() {
        setViewport(m_Viewport);
    }
};

}

#endif
