#ifndef _GLOOPS_VERTEX_ARRAYS_
#define _GLOOPS_VERTEX_ARRAYS_

#include <cstddef>
#include "../GLObject.hpp"

#define GLOOPS_OFFSETOF(TYPE, MEMBER) gloops::bufferOffset(offsetof(TYPE, MEMBER))

namespace gloops {
    
namespace details {
    struct VertexArrayGenerator {
        static GLuint genObject() {
            GLuint id;
            glGenVertexArrays(1, &id);
            GLOOPS_CHECK_ERROR;
            return id;
        }
        
        static void deleteObject(GLuint id) {
            glDeleteVertexArrays(1, &id);
            GLOOPS_CHECK_ERROR;
        }
    };
}

inline const GLvoid* bufferOffset(size_t offset) {
    return reinterpret_cast<const GLvoid*>(offset);
}

typedef GLObject<details::VertexArrayGenerator> VertexArrayObject;

class VertexArrayBind {
public:
    VertexArrayBind(const VertexArrayObject& vertexArray) {
        glBindVertexArray(vertexArray.glId());
        GLOOPS_CHECK_ERROR;
    }
    
    ~VertexArrayBind() {
        glBindVertexArray(0);
        GLOOPS_CHECK_ERROR;
    }
    
    VertexArrayBind(const VertexArrayBind&) = delete;
    VertexArrayBind& operator =(const VertexArrayBind&) = delete;
    
    VertexArrayBind& enableVertexAttribArray(GLuint idx) {
        glEnableVertexAttribArray(idx);
        GLOOPS_CHECK_ERROR;
        return *this;
    }
    
    VertexArrayBind& disableVertexAttribArray(GLuint idx) {
        glDisableVertexAttribArray(idx);
        GLOOPS_CHECK_ERROR;
        return *this;
    }
    
    VertexArrayBind& vertexAttribPointer(GLuint idx, GLint size, GLenum type, GLboolean normalized, 
        GLsizei stride, const GLvoid* pointer) {
        glVertexAttribPointer(idx, size, type, normalized, stride, pointer);
        GLOOPS_CHECK_ERROR;
        return *this;
    }

    VertexArrayBind& vertexAttribIPointer(GLuint idx, GLint size, GLenum type,
        GLsizei stride, const GLvoid* pointer) {
        glVertexAttribIPointer(idx, size, type, stride, pointer);
        GLOOPS_CHECK_ERROR;
        return *this;
    }
    
    const VertexArrayBind& drawArrays(GLenum mode, GLint first, GLsizei count) const {
        glDrawArrays(mode, first, count);
        GLOOPS_CHECK_ERROR;
        return *this;
    }

    const VertexArrayBind& drawElements(GLenum mode, GLsizei count, GLenum type, const GLvoid* indices) const {
        glDrawElements(mode, count, type, indices);
        GLOOPS_CHECK_ERROR;
        return *this;
    }

private:
};

}

#endif
