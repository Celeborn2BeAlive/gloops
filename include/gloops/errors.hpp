#ifndef _GLOOPS_ERRORS_
#define _GLOOPS_ERRORS_

#include <iostream>

//#define GLOOPS_ENABLE_ERROR_CHECK
#ifdef GLOOPS_ENABLE_ERROR_CHECK
#define GLOOPS_CHECK_ERROR gloops::checkGLErrors(__FILE__, __LINE__)
#else
#define GLOOPS_CHECK_ERROR
#endif

namespace gloops {

inline const char* GLErrorString(GLenum error) {
    switch(error) {
    case GL_NO_ERROR:
        return "GL_NO_ERROR: No error has been recorded.";
    case GL_INVALID_ENUM:
        return "GL_INVALID_ENUM: An unacceptable value is specified "
        "for an enumerated argument. The offending command is ignored "
        "and has no other side effect than to set the error flag.";
    case GL_INVALID_VALUE:
        return "GL_INVALID_VALUE: A numeric argument is out of range. "
        "The offending command is ignored and has no other side effect than to set the error flag.";
    case GL_INVALID_OPERATION:
        return "GL_INVALID_OPERATION: The specified operation is not allowed in the current state. "
        "The offending command is ignored and has no other side effect than to set the error flag.";
    case GL_INVALID_FRAMEBUFFER_OPERATION:
        return "GL_INVALID_FRAMEBUFFER_OPERATION: The framebuffer object is not complete. "
        "The offending command is ignored and has no other side effect than to set the error flag.";
    case GL_OUT_OF_MEMORY:
        return "GL_OUT_OF_MEMORY: There is not enough memory left to execute the command. "
        "The state of the GL is undefined, except for the state of the error flags, after this error "
        "is recorded.";
    case GL_STACK_UNDERFLOW:
        return "GL_STACK_UNDERFLOW: An attempt has been made to perform an operation that would "
        "cause an internal stack to underflow.";
    case GL_STACK_OVERFLOW:
        return "GL_STACK_OVERFLOW: An attempt has been made to perform an operation that would cause "
        "an internal stack to overflow.";
    }
    return "";
}

inline bool checkGLErrors(const char* file = nullptr, uint32_t line = 0) {
    GLenum error = glGetError();
    
    if(GL_NO_ERROR == error) {
        return false;
    }
    
    if(file) {
        std::cerr << "in file " << file << ", line " << line << std::endl;
    }
    
    do {
        std::cerr << GLErrorString(error) << std::endl;
    } while(GL_NO_ERROR != (error = glGetError()));
    
    return true;
}

}

#endif
